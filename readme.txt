

== Changelog ==

= 21 June 2019 =
* Sophisticated Business 1.5: various style

= 7 June 2019 =
* Heading Size Adjustment
* Update screenshot to include logo and menus

= 5 June 2019 =
* Sophisticated Business 1.4: contrast fix for featured sticky post tag.

= 4 June 2019 =
* Sophisticated Business 1.3: style

= 29 May 2019 =
* page templates project
* page templates project

= 24 May 2019 =
* Sophisticated Business 1.2 - fix for entry meta widths.

= 21 May 2019 =
* Fixing font-weight for legacy cover block
* Fix font sizes for Cover Block to account for changes in markup

= 20 May 2019 =
* v1.1 style

= 9 May 2019 =
* Various

= 23 April 2019 =
* Fix `.alignwide` Columns Block padding issues

= 22 March 2019 =
* Add Header block styling

= 14 February 2019 =
* Update quote block border styles to work better with the new styles planned for Gutenberg 5.2.

= 1 February 2019 =
* Add fallback for Media & Text block for IE 11, and make sure page title is centred when there are no posts.
* Adding Glotpress project for theme.

= 28 January 2019 =
* Update theme description; update hover colours.

= 25 January 2019 =
* Remove custom colour lightness and satuation filters; this fix has been moved to Twenty Nineteen instead.
* Updating how the hover colour is calculated to better work with custom colours.
* Add lightness and saturation filters to the custom colours, too, to make sure the colours are correct in the editor.

= 24 January 2019 =
* Correct header alignment when in nested blocks.
* Make sure links in the Media & Text block inherit custom colours.
* Make sure full-width Media & Text blocks still sit right next to each other when stacked.
* Remove negative letter spacing.
* Various updates based on feedback: * Make sure button text colour is consistent. * Lighten up separator blocks. * Make sure full-width image block captions are centred. * Optimize screenshot. * Correct icon colour in menu when a featured image is set. * Update editor styles to match front-end.
* Remove unused color-pattens.php.

= 23 January 2019 =
* Add screenshot.
* Renaming theme in files; removing odd character in the print styles, and the encoding.
* Renaming theme directory.
