<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */ ?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<?php // get_template_part( 'template-parts/footer/footer', 'widgets' ); ?>
		<div class="site-info">

			<?php // Footer Nav
			if(has_nav_menu( 'footer' )): ?>
				<nav class="footer-navigation" aria-label="<?php esc_attr_e( 'Footer Menu', 'twentynineteen' ); ?>">
					<?php wp_nav_menu(
						array(
							'theme_location' => 'footer',
							'menu_class'     => 'footer-menu',
							'depth'          => 1,
						)
					); ?>
				</nav>
			<?php endif; ?>

			<?php // Social Nav
			if(has_nav_menu( 'social' )): ?>
				<nav class="social-navigation" aria-label="<?php esc_attr_e( 'Social Links Menu', 'twentynineteen' ); ?>">
					<?php wp_nav_menu(
						array(
							'theme_location' => 'social',
							'menu_class'     => 'social-links-menu',
							'link_before'    => '<span class="screen-reader-text">',
							'link_after'     => '</span>' . twentynineteen_get_icon_svg( 'link' ),
							'depth'          => 1,
						)
					); ?>
				</nav>
			<?php endif; ?>

		</div>
	</footer>

</div><?php // #page ?>

<?php wp_footer(); ?>

</body>
</html>
